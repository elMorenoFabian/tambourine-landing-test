new Swiper('.js-main-slider', {
    loop: true,
    autoplay: {
        delay: 8000,
        disableOnInteraction: false,
    },
    breakpoints: {
        1024: {
            navigation: {
                nextEl: ".js-main-slider-next",
                prevEl: ".js-main-slider-prev",
            },
        }
    }
});

document.addEventListener('click', (event)=> {
    if (event.target.matches('.js-open-modal-btn')) {
        document.querySelector('.js-overlay-modal').classList.add('overlayer-modal_active');
        event.target.closest('.js-book-now-modal').classList.add('book-now-modal_active');
    }
    
    if (event.target.closest('.js-close-modal-btn')) {
        document.querySelector('.js-overlay-modal').classList.remove('overlayer-modal_active');
        event.target.closest('.js-book-now-modal').classList.remove('book-now-modal_active');
    }
});

$(()=> {
    $(".js-form-reservation").validate({
        groups: {
            type: "adult child"
        },
        errorPlacement: function(error, element) {
            element.closest('.field').append(error);
        }
    });
})